<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn-subMenu</name>
   <tag></tag>
   <elementGuidId>638ebbc9-6fa1-4737-a5b7-3c5aa3adf839</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[text()=&quot;${subMenu}&quot;][count(. | //*[@ref_element = 'Object Repository/Web/Frame']) = count(//*[@ref_element = 'Object Repository/Web/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[text()=&quot;${subMenu}&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/Frame</value>
   </webElementProperties>
</WebElementEntity>
