<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>list-menuReport</name>
   <tag></tag>
   <elementGuidId>20b3643f-dc8d-4f0f-b39a-7cb64c4f6abe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;ECSV.dtExportCSV&quot;]/div[2]/div/table/tbody/tr[*]/td/span[text()=&quot;${menuReport}&quot;][count(. | //*[@ref_element = 'Object Repository/Web/Frame']) = count(//*[@ref_element = 'Object Repository/Web/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;ECSV.dtExportCSV&quot;]/div[2]/div/table/tbody/tr[*]/td/span[text()=&quot;${menuReport}&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/Frame</value>
   </webElementProperties>
</WebElementEntity>
