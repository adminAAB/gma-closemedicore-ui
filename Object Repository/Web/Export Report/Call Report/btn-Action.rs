<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn-Action</name>
   <tag></tag>
   <elementGuidId>23c5a145-2311-4943-b496-a642792895e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a2is-popup[1]//button[text()=&quot;${action}&quot;][count(. | //*[@ref_element = 'Object Repository/Web/Frame']) = count(//*[@ref_element = 'Object Repository/Web/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a2is-popup[1]//button[text()=&quot;${action}&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/Frame</value>
   </webElementProperties>
</WebElementEntity>
