<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn-GLTypeSub</name>
   <tag></tag>
   <elementGuidId>4e1d6ec6-90ff-4d41-9c1e-ab0ca9c8215b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;CGR.GLType&quot;]/div[2]/div[1]/div/div/button[text()='${glTypeSub}'][count(. | //*[@ref_element = 'Object Repository/Web/Frame']) = count(//*[@ref_element = 'Object Repository/Web/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;CGR.GLType&quot;]/div[2]/div[1]/div/div/button[text()='${glTypeSub}']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/Frame</value>
   </webElementProperties>
</WebElementEntity>
