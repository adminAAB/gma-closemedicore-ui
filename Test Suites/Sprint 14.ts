<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Sprint 14</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>93a2bb44-f53d-4c94-8aae-87f13acc04a1</testSuiteGuid>
   <testCaseLink>
      <guid>a597a7d1-690a-45fc-9bb3-7e63d6f20686</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Call Report/001-Call Data null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d598a7a9-9a2f-497f-a3ce-e657525a32fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Call Report/002-Call DtFrom</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ba361d3d-3d21-4056-9e29-6448fa0ddefc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Call Report/003-Call DtUntil</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9c9a25e-8433-48b2-80b3-d30a92ec7fd4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Call Report/004-Call CallType</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6dcd4fa2-8cf1-4da2-bf6e-97598a6c14ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Call Report/005-Call ServiceType</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c63287b-29f9-4d1d-bc4e-111c02969b0a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Call Report/006-Call DtFrom,DtUntil</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9ddf1148-277d-408a-bfbe-87ffab83972c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Call Report/007-Call DtFrom,CallType</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e9e46c0-2caa-469d-86eb-d47a3ca40d97</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Call Report/008-Call DtFrom,ServiceType</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b0cc6b7-15d0-4977-a05d-0431b52cd0a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Call Report/009-Call DtUntil,CallType</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0233b236-fe39-43d2-9009-e165447fda53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Call Report/010-Call DtUntil,ServiceType</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa0df546-cd63-4ccf-802b-162f9ac71a17</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Call Report/011-Call DtFrom,DtUntil,CallType</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9383cc7-19b9-4cd3-93df-f3c156667a09</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Call Report/012-Call DtFrom,DtUntil,ServiceType</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3094c517-226e-418b-a419-b662367c6ea4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Call Report/013-Call DtUntil,CallType,ServiceType</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3cbcdb2-2055-47ef-885a-c28b66710378</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Call Report/014-Call All Parameter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a822ec8b-3060-4479-a06e-a41634649e07</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Call Report/015-Call CallType,ServiceType</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6912f29d-1ae5-440f-b2c2-4beefb95e9af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Call Report/016-Call DtFrom,CallType,ServiceType</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>72af57c5-6897-4dd2-b523-1d77ae0813fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Claim Report/001-Claim Data null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>61a66a87-4899-4f48-aa14-6e5e06618186</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Claim Report/002-Claim DtFrom</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e5c67396-595e-4aa4-91d7-8b0c39160d11</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Claim Report/003-Claim DtUntil</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c99c2905-d450-4208-a59e-7f288b32d011</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Claim Report/004-Claim Product</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>847aca36-f483-4d19-8bbc-65f89b13f563</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Claim Report/005-Claim GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1fcaf7c0-96ae-48d7-9172-f147b7769977</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Claim Report/006-Claim DtFrom,DtUntil</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d24db0bf-cea9-4607-951c-983da2326a8f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Claim Report/007-Claim DtFrom,Product</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>206fde41-815c-4f14-9672-a1ce4eb16cbe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Claim Report/008-Claim DtFrom,GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b7513a2e-8d2d-4160-8d08-a133c89694be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Claim Report/009-Claim DtUntil,Product</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d924ec17-0707-4657-8745-6c69bce77cbb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Claim Report/010-Claim DtUntil,GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1256d13c-9388-435d-820e-565044ec11e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Claim Report/011-Claim Product,GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ef2c7cbd-629e-4aad-baf0-6dd677c6d8a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Claim Report/012-Claim DtFrom.DtUntil,Product</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe2feb3c-b7c0-4b34-9783-0c4b96583086</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Claim Report/013-Claim DtFrom.DtUntil,GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b6f34c5-f9dd-4ce0-af77-985a6cd002f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Claim Report/014-Claim DtUntil,Product,GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5712ec9b-bace-47e6-803e-8c9bc012d23d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Claim Report/015-All Parameter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db41cf67-b477-48ce-adca-223b11769d9e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Export Claim Report/016-Claim DtFrom,Product,GL</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
