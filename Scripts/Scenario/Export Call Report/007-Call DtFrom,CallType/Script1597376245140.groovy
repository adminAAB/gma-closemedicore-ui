import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String strMenuReport = 'Call Report'
String strDateFrom = 'OK'
String strDateUntil = ''
String strCallType = 'Callout'
String strServiceType = ''

WebUI.callTestCase(findTestCase('Web/Login/Login'), [('username') : 'REM',('password') : 'Password95'], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.click(findTestObject("Web/Home/btn-CTI OK"))

WebUI.callTestCase(findTestCase('Web/Home/Click Menu'), [('menu') : 'Export',('subMenu') : 'Export to CSV'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/Export Report/Pilih Menu Report'), [('menuReport') : strMenuReport,('action') : 'Select'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/Export Report/Call Report/Export Call Report'), [('dateFrom') : strDateFrom,
	('dateUntil') : strDateUntil, ('callType') : strCallType, ('serviceType') : strServiceType], FailureHandling.STOP_ON_FAILURE)

//

