import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

/*DATA DARI LUAR
 * menu = pilihan menu report
 * dateFrom = set tanggal awal
 * dateUntil = set tanggal akhir
 * callType = tipe caller (call in/callout)
 * serviceType = tipe perawatan
*/
	if (dateFrom != '') {
		WebUI.click(findTestObject("Web/Export Report/Call Report/btn-DateFrom"))
		WebUI.click(findTestObject("Web/Export Report/Call Report/txt-DateFrom"))
	}
	if (dateUntil != '') {
		WebUI.click(findTestObject("Web/Export Report/Call Report/btn-DateUntil"))
		WebUI.click(findTestObject("Web/Export Report/Call Report/txt-DateFrom"))
	} 
	if (callType != '') {
		WebUI.click(findTestObject("Web/Export Report/Call Report/btn-CallType"))
		WebUI.click(findTestObject("Web/Export Report/Call Report/btn-CallTypeSub",["callTypeSub" : callType]))
	}
	if (serviceType != '') {
		WebUI.click(findTestObject("Web/Export Report/Call Report/btn-ServiceType"))
		WebUI.click(findTestObject("Web/Export Report/Call Report/btn-ServiceTypeSub",["serviceTypeSub" : serviceType]))
	}
	
	WebUI.click(findTestObject("Web/Export Report/Call Report/btn-Action",["action" : 'Export']))
	WebUI.delay(3)
	if (dateFrom == '' || dateUntil == '') {
		
		WebUI.verifyElementText(findTestObject("Web/Export Report/Call Report/txt-Validation"), 'Call Date From')
	}